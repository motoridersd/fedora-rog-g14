# CONFIG_TIMERLAT_TRACER:
# 
# The timerlat tracer aims to help the preemptive kernel developers
# to find sources of wakeup latencies of real-time threads.
# 
# The tracer creates a per-cpu kernel thread with real-time priority.
# The tracer thread sets a periodic timer to wakeup itself, and goes
# to sleep waiting for the timer to fire. At the wakeup, the thread
# then computes a wakeup latency value as the difference between
# the current time and the absolute time that the timer was set
# to expire.
# 
# The tracer prints two lines at every activation. The first is the
# timer latency observed at the hardirq context before the
# activation of the thread. The second is the timer latency observed
# by the thread, which is the same level that cyclictest reports. The
# ACTIVATION ID field serves to relate the irq execution to its
# respective thread execution.
# 
# The tracer is build on top of osnoise tracer, and the osnoise:
# events can be used to trace the source of interference from NMI,
# IRQs and other threads. It also enables the capture of the
# stacktrace at the IRQ context, which helps to identify the code
# path that can cause thread delay.
# 
# Symbol: TIMERLAT_TRACER [=n]
# Type  : bool
# Defined at kernel/trace/Kconfig:393
#   Prompt: Timerlat tracer
#   Depends on: TRACING_SUPPORT [=y] && FTRACE [=y]
#   Location:
#     -> Kernel hacking
#       -> Tracers (FTRACE [=y])
# Selects: OSNOISE_TRACER [=n] && GENERIC_TRACER [=y]
# 
# 
# 
# CONFIG_TIMERLAT_TRACER is not set
